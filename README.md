# CWU Database  
This is a web application to manage a database of civilian. It was made for a serious RP Half Life server on the game Garry's Mod.

>The Civil Worker's Union (CWU) is a labor organization created and fostered by the Universal Union following their occupation of Earth after the Seven Hour War. The Union provides a type of role-play unlike some of the other factions, offering a role of distributing, purchasing, and selling goods to other citizens. There is a plethora of routes to take with a character who is employed by the CWU, many range from taking a strict loyalist route, smuggling goods to anti-citizens, or somewhere in between.
>From http://hl2rp.wikia.com/wiki/Civil_Worker%27s_Union

## The database  
An owner of a Garry's Mod Half Life server can rent a database. Each user are added by an administrator account.
The user can add a civilian or edit it. Only the admin account can delete it.
Each civilian can be marked as researched, lost or dead.

## "Hacking" the database
In the roleplay scenario, some civilians are rebilious to the Union. Those civilian can obtain a working special code from a valid Union Worker and use this code to
"hack" the database. By using the user "root" and the code as the password, the Rebels are allowed to make 3 changes to the database. The goal is to let the rebel create a fake identity or edit one (mark it as lost/unlost, etc.).

## Note
The project was originaly made using vanilla PHP (no framework) but I tryed to remake it using Node.js to be more modern.
