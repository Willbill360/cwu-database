var mysql = require('mysql');

var pool = mysql.createPool({
	connectionLimit: 10,
	host: process.env.DBHOST || "localhost",
	user: process.env.DBUSER || "root",
	password: process.env.DBPASS || "",
	database: process.env.DBNAME || "cwudb"
});

// TODO: TEST this not sure it works
module.exports = function () {

	function _query(query, params, callback) {
		pool.getConnection(function (err, connection) {
			if (err) {
				connection.release();
				callback(null, err);
				throw err;
			}

			connection.query(query, params, function (err, rows) {
				console.log(this.sql);
				connection.release();
				if (!err) {
					callback(rows);
				}
				else {
					callback(null, err);
				}
			});

		});
	};

}